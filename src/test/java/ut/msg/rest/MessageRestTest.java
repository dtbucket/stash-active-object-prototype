package ut.msg.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import msg.rest.MessageRest;
import msg.rest.MessageRestModel;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.GenericEntity;

public class MessageRestTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {
        MessageRest resource = new MessageRest();

        Response response = resource.getMessage();
        final MessageRestModel message = (MessageRestModel) response.getEntity();

        assertEquals("wrong message","Hello World",message.getMessage());
    }
}
