package it.msg.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import msg.rest.MessageRest;
import msg.rest.MessageRestModel;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class MessageRestFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/messagerest/1.0/message";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        MessageRestModel message = resource.get(MessageRestModel.class);

        assertEquals("wrong message","Hello World",message.getMessage());
    }
}
