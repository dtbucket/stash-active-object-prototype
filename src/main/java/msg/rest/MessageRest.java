package msg.rest;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import static com.google.common.base.Preconditions.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */
@Path("/message")
public class MessageRest {
	private final ActiveObjects ao;
	public MessageRest(){
		ao = null;
	}
	public MessageRest(ActiveObjects ao){
		this.ao = checkNotNull(ao);
	}

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage() {
       return Response.ok(new MessageRestModel(ao)).build();
    }
    
    @POST
    @Path("/store")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getPostMessage(@QueryParam("message") String message) {
    	return Response.ok(new MessageRestModel(message, ao)).build();
    }
}