package msg.rest;

import javax.xml.bind.annotation.*;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;


@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageRestModel {

    @XmlElement(name = "value")
    private String message;

    public MessageRestModel(final ActiveObjects ao) {
    	//this.ao = checkNotNull(ao);
		ao.executeInTransaction(new TransactionCallback<String>()
		{
			@Override
			public String doInTransaction() {
				for (MessageEntity msg : ao.find(MessageEntity.class))
				{
					return msg.getMessage();
				}
				return "";
			}
		});
    }

	public MessageRestModel(final String message, final ActiveObjects ao) {
        this.message = message;
    	//this.ao = checkNotNull(ao);
		ao.executeInTransaction(new TransactionCallback<MessageEntity>() // (1)
		{
			@Override
			public MessageEntity doInTransaction() {
				final MessageEntity msg = ao.create(MessageEntity.class); // (2)
				msg.setDescription(message);
				msg.save(); // (4)
				return msg;
			}
		});
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}