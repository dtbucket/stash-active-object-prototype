package msg.rest;

import net.java.ao.Entity;
import net.java.ao.Preload;
 
@Preload
public interface MessageEntity extends Entity {
    String getMessage();
    
    void setDescription(String description);
 
    boolean isComplete();
 
    void setComplete(boolean complete);
}
